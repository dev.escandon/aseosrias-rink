const objMentor = {
  name: 'Israel',
  apellido: 'Salinas',
  edad: 30,
  tieneMascota: true,
}

const otroObjeto = { ...objMentor }

console.log(objMentor)
console.log(otroObjeto)

const {name, ...restData } = objMentor

console.log(name)
console.log(restData)

