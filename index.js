// Definición
function elevarCuadrado(array) {
    array.forEach((element, index) => {
        console.log(element*element)
        console.log(index)
    });
}

//Llamado de la función
elevarCuadrado([34, 56, 78, 67,89])

// CE: Cualquier nombre con la letra A minúscula o mayúscula
// CS: Eliminar todos los item que no cumplan dicha condición

const arrayNames = ["Alma","agustin", "Mario"]

const newArray = arrayNames.filter(name=>{
    const splitString = name.split("")
    if(splitString[0]=='a' || splitString[0]=='A') return item
})

console.log(newArray)


