const arrayMentor = {
  name: 'Israel',
  apellido: 'Salinas',
  edad: 30,
  tieneMascota: true,
}

// Destructuración
console.log(arrayMentor.name)
console.log(arrayMentor.apellido)

const nameLarge = arrayMentor.name

console.log(nameLarge)

const { name } = arrayMentor
console.log(name)
